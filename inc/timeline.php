<?php
//get the simplepie library
// Create a new SimplePie object
require_once('../vendor/simplepie/autoloader.php');

//grab the feeds
$feed = new SimplePie();
$feed->set_curl_options(
	array(
		CURLOPT_SSL_VERIFYHOST => false,
		CURLOPT_SSL_VERIFYPEER => false)
);
$feed->set_feed_url(array(
	'https://www.tifred.fr/feed/',
	'https://shaarli.tifred.fr/?do=rss',
//	'http://twitrss.me/twitter_user_to_rss/?user=tifredfr',
    'https://nitter.net/search/rss?f=tweets&q=tifredfr',
	'https://github.com/tifredfr.atom',
	'https://gitlab.com/tifredfr.atom',
  'https://framasphere.org/public/tifredfr.atom',
  'https://framagit.org/tifredfr.atom',
));
//enable caching
$feed->enable_cache(true);
//provide the caching folder
$feed->set_cache_location('../cache');
//set the amount of seconds you want to cache the feed
$feed->set_cache_duration(1800);
$feed->set_stupidly_fast(true);
$feed->strip_htmltags(true);
$feed->remove_div(true);
$feed->enable_order_by_date(true);
//init the process
$feed->init();
//let simplepie handle the content type (atom, RSS...)
$feed->handle_content_type();
//set number of items to be displayed
$total_articles = 10;
for ($x = 0; $x < $feed->get_item_quantity($total_articles); $x++)
{
    $lifestream_items[] = $feed->get_item($x);
}
?>
<ul class="collection">
    <?php foreach ($lifestream_items as $item): ?>
			<li class="collection-item">
				<img src="https://www.google.com/s2/favicons?domain_url=<?php echo $item->get_permalink(); ?>" alt="Favicon"/> <span class="title"><a href="<?php echo $item->get_permalink(); ?>"><?php echo $item->get_title(); ?></a></span> <small>( le <?php echo $item->get_date('j M Y'); ?> à <?php echo $item->get_date('G:i'); ?> )</small>
      </li>
    <?php endforeach; ?>
</ul>
