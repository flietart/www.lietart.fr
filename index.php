<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
	<title>Frédéric LIÉTART - Administrateur système GNU/Linux (Orléans, France)</title>
  <meta name="description" content="A propos de Frédéric LIÉTART - Administrateur système GNU/Linux (Orléans, France) - Technicien de maintenance informatique Apple certifié. Bienvenue sur ma page personelle.">
	<meta name="keywords" content="frederic lietart, frederic lietard, lietart, lietard, frederic, a propos, apropos, about, thelinux, thelinuxfr, thelinuxfrorg, tifred, tifredfr, cv, Curriculum vitæ, Curriculum, administrateur, administrateur système, administrateur reseau, sysadmin, linux, gnu, debian, archlinux, fedora, redhat, administrateur système GNU/Linux, technicien certifié apple, centre de services, admin, jeune motivé, orleans, france, centre, centre val de loire">
	<meta name="author" content="Frédéric LI&#201;TART">
	<meta name="robots" content="all" />
	<link rel="canonical" href="https://www.lietart.fr/" />
  <link rel="shortcut icon" href="img/favicon.png">

	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:description" content="A propos de Frédéric LIÉTART - Administrateur système GNU/Linux (Orléans, France)"/>
	<meta name="twitter:title" content="Frédéric LIÉTART - Administrateur système GNU/Linux (Orléans, France)"/>
	<meta name="twitter:site" content="@tifredfr"/>
	<meta name="twitter:domain" content="lietart.fr"/>

	<meta property="og:locale" content="fr_FR"/>
	<meta property="og:type" content="website"/>
	<meta property="og:title" content="Frédéric LIÉTART - Administrateur système GNU/Linux (Orléans, France)"/>
	<meta property="og:description" content="A propos de Frédéric LIÉTART - Administrateur système GNU/Linux (Orléans, France) - Technicien de maintenance informatique Apple certifié. Bienvenue sur ma page personelle." />
	<meta property="og:url" content="https://www.lietart.fr/"/>
	<meta property="og:site_name" content="Frédéric LI&#201;TART - Administrateur système GNU/Linux (Orléans, France)"/>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
  <!--Import Roboto Font-->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet" type="text/css">
  <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <!-- Font Awesome -->
  <script src="https://kit.fontawesome.com/8ed683b72a.js" crossorigin="anonymous"></script>
</head>

<body id="index">
<!-- Fixed navbar -->
<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
    <li><a href="#CV-EXP">Expériences professionnelles</a></li>
    <li><a href="#CV-Formations">Formations</a></li>
    <li><a href="#CV-CMP-Info">Compétences informatiques</a></li>
    <li><a href="#CV-CMP-Indus">Compétences industrielles</a></li>
    <li><a href="#CV-Autres">Autres compétences</a></li>
    <li><a href="#CV-Loisirs">Loisirs</a></li>
</ul>

<div class="navbar-fixed">
    <nav class="black" role="navigation">
        <div class="nav-wrapper container">
            <a href="https://www.lietart.fr/" class="brand-logo right">Frédéric LI&#201;TART</a>
            <ul id="nav-mobile" class="left hide-on-med-and-down">
                <li><a href="https://www.lietart.fr/">Accueil</a></li>
                <li><a href="#activite" title="Activités sociales">Activités sociales</a></li>
                <!-- Dropdown Trigger -->
                <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Curriculum vitæ<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a href="#liens" title="Divers liens autour du libre">Liens</a></li>
            </ul>
        </div>
    </nav>
</div>
<!-- /Fixed navbar -->

<!-- Banner -->
<div id ="banner" class="banner parallax-container z-depth-4 valign-wrapper">
    <div class="container center valign">
      <h4 class="banner-header center light">
        <span class="white-text">Bienvenue sur ma page personnelle, permettant de regrouper divers liens vers mes réseaux sociaux, curriculum vitæ et biographie.</span>
      </h4>
        <a href="https://www.twitter.com/tifredfr" title="Suivez moi sur Twitter" target="_blank"><i class="fab fa-twitter fa-inverse fa-4x"></i></a>
  			<a href="https://www.linkedin.com/in/fredericlietart" title="Suivez moi sur Linkedin" target="_blank"><i class="fab fa-linkedin fa-inverse fa-4x"></i></a>
  			<a href="https://gitlab.com/tifredfr" title="Profil GitLab" target="_blank"><i class="fab fa-gitlab fa-inverse fa-4x"></i></a>
  			<a href="https://github.com/tifredfr" title="Profil GitHub" target="_blank"><i class="fab fa-github fa-inverse fa-4x"></i></a>
  			<a href="https://www.facebook.com/frederic.lietart" title="Suivez moi sur FaceBook" target="_blank"><i class="fab fa-facebook fa-inverse fa-4x"></i></a>
  			<a href="http://www.lastfm.fr/user/flietart" title="Profil LastFM" target="_blank"><i class="fab fa-lastfm fa-inverse fa-4x"></i></a>
  			<a href="#liens" title="Plus de liens"><i class="fa fa-plus-square fa-inverse fa-4x"></i></a>
    </div>
    <div class="parallax">
        <img src="img/parallax.jpg" alt="parallax1" />
    </div>
</div>
<!-- /banner -->

<!-- Welcome -->
<div id="welcome" class="welcome indigo darken-3 white-text z-depth-2">
    <div class="container">
        <div class="row">
            <div class="col s12 m8">
                <p class="text-justify lead text-muted" style="text-align:justify;"></p>
                <p class="text-justify" style="text-align:justify;">
                    Passionné d'informatique depuis mon plus jeune âge, j'ai eu la chance de pouvoir faire mes premières armes sur divers matériels plus exotiques les uns que les autres comme des
                    <abbr title="SUN SPARCstation 10">Sun SS10</abbr>, des boîtiers SCSI à plus en finir… Au fil de mes expériences j'ai pu acquérir une certaine expérience informatique, jusqu'à trouver sur mon chemin ma première distribution GNU/Linux
                    (Mandrake Linux 6.1) en 1999.</p>
                <p class="text-justify" style="text-align:justify;">Voilà le déclencheur, de là ma principale occupation fut d'en apprendre plus sur ce système d'exploitation et de toute la sphère environnante. L’expérience ne venant pas toute seule, je ne peux compter les heures passées sur mes systèmes à essayer de
                    monter telles ou telles solutions, mais c'est comme cela que j'ai appris, et l'envie est toujours présente après des années. C'est maintenant mon activité professionnelle.</p>
                <p class="text-justify" style="text-align:justify;">N'hésitez pas à visiter mon blog sur l'informatique <a href="https://www.tifred.fr/" target="_blank">www.tifred.fr</a>.</p>
            </div>

            <div class="col s12 m4 indigo darken-1">
            <p>
              <div class="right">
                <img src="img/myAvatar.png" class="responsive-img z-depth-2" style="max-width: 100px; display: block; margin-left: auto; margin-right: auto; border-radius: 5px;" alt="Photo">
              </div>
              <address>
              <b>Frédéric LI&#201;TART</b>
              <small>(né en 1987)</small><br>
              <br><b>Administrateur système Linux</b>
              <br>Orléans, Centre-Val de Loire, France<br>
              <br>
              <ul class="fa-ul">
                <li><abbr title="Email"><i class="fa-li fa fa-envelope"></i></abbr> frederic <i>AT</i> lietart.fr</li>
                <li><abbr title="Site Web"><i class="fa-li fa fa-globe"></i></abbr> <a href="https://www.tifred.fr/" target="_blank">www.tifred.fr</a></li>
                <li><abbr title="PGP Public Key"><i class="fa-li fa fa-key"></i></abbr> <a href="inc/PGP.asc">PGP Public Key</a></li>
              </ul>
              </address>
            </p>
            <p><b><small>Pseudo : <a href="https://www.google.fr/search?q=tifredfr">TiFredFr</a> - <a href="https://www.google.fr/search?q=thelinuxfr">thelinuxfr</a> - <a href="https://www.google.fr/search?q=thelinuxfrOrg">thelinuxfrOrg</a></small></b></p>
          </div>
        </div>
    </div>

<!-- SKILL -->
<div id="skill" class="skill col s12 indigo darken-4">
  <div class="container">
    <div class="row">
      <div class="col s12 ">
        <p><b>Compétences</b></p>
          <div class="chip">Debian</div>
          <div class="chip">CentOS</div>
          <div class="chip">Oracle Linux</div>
          <div class="chip">Fedora</div>
          <div class="chip">Ubuntu</div>
          <div class="chip">macOS</div>
          <div class="chip">DHCP</div>
          <div class="chip">DNS</div>
          <div class="chip">HTTP</div>
          <div class="chip">Samba</div>
          <div class="chip">GLPI</div>
          <div class="chip">OTRS</div>
          <div class="chip">Docker</div>
          <div class="chip">Router</div>
          <div class="chip">Firewall</div>
          <div class="chip">OPNsense</div>
          <div class="chip">pfSense</div>
          <div class="chip">Zabbix</div>
          <div class="chip">HTML</div>
          <div class="chip">Git</div>
          <div class="chip">CSS</div>
          <div class="chip">CMS</div>
          <div class="chip">Google Workspace</div>
          <div class="chip">Microsoft 365</div>
          <div class="chip">OVH</div>
          <div class="chip">Online</div>
          <div class="chip">Gandi</div>
          <div class="chip">Social</div>
          <div class="chip">Gentoo</div>
          <div class="chip">ArchLinux</div>
          <div class="chip">Windows</div>
          <div class="chip">LDAP</div>
          <div class="chip">FTP</div>
          <div class="chip">Plesk</div>
          <div class="chip"><a href="#CV-CMP-Info" title="Plus de compétences"><i class="tiny material-icons">add</i></a></div>
      </div>
    </div>
  </div>
</div>
<!-- /SKILL -->

</div>
<!-- /Welcome -->

<!-- Timeline -->
<div id="activite" class="activite timeline">
  <div class="section no-pad-bot">
    <div class="container">
      <div class="row">
        <h2 class="center-align"><b>Activités sociales</b><div class="progress"><div class="indeterminate"></div></div></h2>
        <div class="showtimeline">
          <!-- include jquery -->
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /timeline -->

<!-- CV -->
<div class="cv">
<div id="CV-EXP" class="container">
<div class="row">

<div class="section center-align">
  <a href="inc/CV.pdf" title="Télécharger mon CV au format PDF" class="btn-floating waves-effect waves-light red darken-2 right"><i class="material-icons right">file_download</i>CV PDF</a>
  <h2>EXPÉRIENCES PROFESSIONNELLES</h2>
</div>
<div class="col s12 m6">

<ul class="collapsible grey lighten-5" data-collapsible="accordion">
    <li>
        <div class="collapsible-header active indigo darken-4 white-text z-depth-2"><i class="material-icons">work</i> <b>Technimac / iStack (Orléans, Loiret, France)</b>
            <small>novembre 2009 à nos jours</small>
        </div>
        <div class="collapsible-body">
            <p><i>Technicien de maintenance informatique</i>
                <br />
                <u>CDI</u> : Maintenance matériel et système sur la gamme des produits Apple. Ingénierie informatique (serveur, stockage, réseau et sécurité).
            </p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Migration Microsoft 365
            <small>décembre 2020</small>
        </div>
        <div class="collapsible-body">
            <p>Migration chez l’un de nos clients vers Microsoft 365.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Migration serveur Debian
            <small>juin 2020</small>
        </div>
        <div class="collapsible-body">
            <p>Migration d'un serveur utilisé pour l'herbergement (web, mail, DNS), vers une nouvelle infrastructure fonctionnant toujours sous Debian.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Installation d'un NAS dans une TPE
            <small>décembre 2019</small>
        </div>
        <div class="collapsible-body">
            <p>Mise en place dans une TPE d'un NAS Synology afin de pouvoir créer un "cloud" personnel et faciliter une utilisation à distance via divers périphériques</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Renouvellement de parc dans une association.
            <small>octobre 2019</small>
        </div>
        <div class="collapsible-body">
            <p>Renouvellement système et logiciel du parc informatique dans une association ratachée à l'état. Remise en conformité du serveur, migration de calendriers, constats et mails vers OVH Exchange.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Migration Microsoft Office 365
            <small>août 2019</small>
        </div>
        <div class="collapsible-body">
            <p>Migration chez l’un de nos clients vers Office 365 afin de remplacer un ancien hébergement de courrier et un DropBox.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Renouvellement de postes cabinet dentaire
            <small>juillet 2019</small>
        </div>
        <div class="collapsible-body">
            <p>Finalisation de la dernière étape concernant le renouvellement du parc informatique d'un cabinet dentaire. Installation de nouveaux postes, écrans de contrôle, imprimantes et éléments réseau.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Migration G Suite
            <small>août 2018</small>
        </div>
        <div class="collapsible-body">
            <p>Migration chez l’un de nos clients PME vers G Suite Business, afin de remplacer un hébergement de courrier obsolète et de transférer les données de l'entreprise dans les nuages (en concervant des sauvegardes internes).</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Mise en place d'OPNsense
            <small>août 2017</small>
        </div>
        <div class="collapsible-body">
            <p>Mise en production chez l’un de nos clients d’OPNsense afin d’avoir un contrôle sur le trafic réseau et pouvoir identifier le moindre problème (téléchargement, intrusion, flux).</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Mise en place d'OTRS
            <small>juin 2017</small>
        </div>
        <div class="collapsible-body">
            <p>Mise en production d'OTRS pour utilisation interne et mise à disposition de nos clients.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Mise en place de Nextcloud
            <small>juin 2016</small>
        </div>
        <div class="collapsible-body">
            <p>Mise en production de Nextcloud pour utilisation interne et mise à disposition de nos clients.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Mise en place de pfSense
            <small>mars 2016</small>
        </div>
        <div class="collapsible-body">
            <p>Mise en production de pfSense (Firewall) pour une utilisation interne avec deux lignes ADSL (FailOver, Balancing).</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Installation serveur OwnCloud
            <small>janvier 2016</small>
        </div>
        <div class="collapsible-body">
            <p>Mise en production d’un serveur CentOS pour l’utilisation de OwnCloud pour une TPE. La machine ayant été installée par un autre prestataire de façon non fiable, il a fallut sauvegarder toutes les données pour ensuite repartir sur un système
                propre CentOS puis réintégrer OwnCloud.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Mise en place Redmine
            <small>février 2014</small>
        </div>
        <div class="collapsible-body">
            <p>Mise en place de Redmine et Git pour notre usage interne afin d’avoir un suivi précis de nos divers développements.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Réseau clinique sur Orléans
            <small>novembre 2013</small>
        </div>
        <div class="collapsible-body">
            <p>Déploiement réseau sur un secteur à base de switchs Cisco SG500-52 en stack - Configuration avancée VLANs et coeur de réseau clinique.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Mise en place GLPI
            <small>octobre 2012</small>
        </div>
        <div class="collapsible-body">
            <p>Mise en production de GLPI pour notre usage interne et le suivi de nos clients. Couplé par la suite de l’agent FusionInventory afin d’avoir un inventaire fiable des serveurs clients en production.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Développement site iStack
            <a class="fa fa-external-link" href="http://www.istack.fr/" target="_blank"></a>
            <small>février 2012</small>
        </div>
        <div class="collapsible-body">
            <p>Mise en place du site internet de l’entreprise sous Joomla avec des modifications importantes au niveau du template.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Mise en place serveur dédié OVH
            <a class="fa fa-external-link" href="http://eta.istack.fr/" target="_blank"></a>
            <small>février 2012</small>
        </div>
        <div class="collapsible-body">
            <p>Installation, configuration et migration des services WEB/DNS sur notre nouveau serveur dédié afin d’accueillir au mieux nos clients professionnels. Fonctionnant sous Debian / ISPConfig et hébergé par OVH.</p>
        </div>
    </li>
    <li>
        <div class="collapsible-header">Externalisation de donnée via 2 NAS
            <small>novembre 2011</small>
        </div>
        <div class="collapsible-body">
            <p>Mise en place d’une sauvegarde externalisée d’environ 5 To via deux NAS NetGear RNDP6000</p>
        </div>
    </li>
</ul>
</div>
<div class="col s12 m6">
<ul class="collapsible grey lighten-5" data-collapsible="accordion">
    <li>
        <div class="collapsible-header active indigo darken-4 white-text z-depth-2"><i class="material-icons">work</i> <b>CONORM (Pierres, Eure et Loir, France)</b>
            <small>octobre 2008 à juin 2009</small>
        </div>
        <div class="collapsible-body">
            <p><i>Régleur/Opérateur sur centre d'usinage (5 axes)</i>
                <br />
                <u>Mission intérim effectuée (nuit)</u> : Usinage de pièces de précisions pour l'aéronautique, contrôle de pièces usinées à l'aide d'instruments de mesure de précision.
            </p>
        </div>
    </li>
</ul>
</div>
<div class="col s12 m6">
<ul class="collapsible grey lighten-5" data-collapsible="accordion">
    <li>
        <div class="collapsible-header active indigo darken-4 white-text z-depth-2"><i class="material-icons">work</i> <b>DUTHION (Yermenonville, Eure et Loir, France)</b>
            <small>septembre 2008</small>
        </div>
        <div class="collapsible-body">
            <p><i>Opérateur sur centre d'usinage(4 axes)</i>
                <br />
                <u>Mission intérim effectuée (équipe)</u> : Usinage pièce pour GIMA<br /> Ébavurages, contrôle de pièces usinées à l'aide d'instruments de mesure de précision.
            </p>
        </div>
    </li>
</ul>
</div>
<div class="col s12 m6">
<ul class="collapsible grey lighten-5" data-collapsible="accordion">
    <li>
        <div class="collapsible-header active indigo darken-4 white-text z-depth-2"><i class="material-icons">work</i> <b>ALSTOM (St-Ouen, Paris, France)</b>
            <small>juillet 2005</small>
        </div>
        <div class="collapsible-body">
            <p><i>Gestion de parc informatique ITC</i>
                <br />
                <u>Mission intérim effectuée</u> : Gestion du parc informatique ALSTOM<br /> Recensement du matériel informatique (machine et périphérique) des utilisateurs. Identification par « code barre » autocollant.
            </p>
        </div>
    </li>
</ul>
</div>
<div class="col s12 m6">
<ul class="collapsible grey lighten-5" data-collapsible="accordion">
    <li>
        <div class="collapsible-header active indigo darken-4 white-text z-depth-2"><i class="material-icons">work</i> <b>DUTHION (Yermenonville, Eure et Loir, France)</b>
            <small>juillet 2005</small>
        </div>
        <div class="collapsible-body">
            <p>
                <u>Stage</u> : Stage d'observation pour validation du BEP MPMI.
            </p>
        </div>
    </li>
</ul>
</div>
</div><!-- END row -->
</div><!-- END container -->


<div id="CV-Formations" class="formations container">
  <div class="row">
    <div class="section indigo darken-4 white-text z-depth-2">
      <h2>FORMATIONS & CERTIFICATIONS</h2>
    </div>
    <table class="striped grey lighten-5 z-depth-2">
      <tbody>
        <tr>
          <td class="cell-left">2021</td>
          <td><strong>Certification sur l'ensemble des produits Apple</strong></td>
        </tr>
        <tr>
          <td class="cell-left">2014</td>
          <td><strong>Small and Midsize Business Engineer & Account Manager</strong><br />Cisco Select Small and Midsize Business</td>
        </tr>
        <tr>
          <td class="cell-left">2013</td>
          <td><strong>SMB Account Manager Certification & Engineer Cisco</strong><br />Cisco Select Small Business</td>
        </tr>
        <tr>
          <td class="cell-left">2012</td>
          <td><strong>Apple MacBook Pro Rétina Qualification</strong><br />9L0-S02</td>
        </tr>
        <tr>
          <td class="cell-left">2012</td>
          <td><strong>Apple Mac OS X 10.7 Service </strong><br />9L0-354 & 9L0-314 : Recertification Exam</td>
        </tr>
        <tr>
          <td class="cell-left">2011</td>
          <td><strong>Apple Mac OS X 10.6 Service</strong><br />9L0-53 & 9L0-313 : Recertification Exam</td>
        </tr>
        <tr>
          <td class="cell-left">2009</td>
          <td><strong>Apple Certified Macintosh Technician</strong><br />Hardware and Mac OS X 10.5 Troubleshooting (Agnosys, Paris)</td>
        </tr>
        <tr>
          <td class="cell-left">2008</td>
          <td><strong>BAC Technologique STI Génie Mécanique</strong><br />Lycée Jehan de Beauce (Chartres, France)</td>
        </tr>
        <tr>
          <td class="cell-left">2005</td>
          <td><strong>BEP Métier de la Production Mécanique Informatisée (MPMI)</strong><br />Lycée Jehan de Beauce (Chartres, France)</td>
        </tr>
      </tbody>
    </table>

    <div id="CV-CMP-Info" class="section indigo darken-4 white-text z-depth-2">
      <h2>COMPÉTENCES INFORMATIQUES</h2>
    </div>
    <table class="striped grey lighten-5 z-depth-2">
      <tbody>
        <tr>
          <td class="cell-left">Système d'exploitation</td>
          <td>Debian, CentOS, Fedora, Ubuntu, Raspbian, ArchLinux, Gentoo<br />MacOS X<br />Windows</td>
        </tr>
        <tr>
          <td class="cell-left">Administration système</td>
          <td>DHCP, DNS, OpenLDAP, FTP, Apache, NGINX, Samba, Netatalk, ISPConfig, GLPI, OTRS, UFW, systemd, Fail2ban, Docker, Plesk...</td>
        </tr>
        <tr>
          <td class="cell-left">Cloud computing</td>
          <td>Dropbox, Google Drive, OneDrive, OwnCloud/Nextcloud<br />Google Cloud Platform<br />Google Workspace, Microsoft 365</td>
        </tr>
        <tr>
          <td class="cell-left">Réseaux</td>
          <td>Router, Firewall, KVM<br />Firewall logiciel (pfSense, OPNsense)<br />Zabbix<br />Câblage réseau, brassage, baie</td>
        </tr>
        <tr>
          <td class="cell-left">Programmation</td>
          <td>HTML, CSS, GIT<br />Connaissances : PHP, Ruby, Bash, Python</td>
        </tr>
        <tr>
          <td class="cell-left">Virtualisation</td>
          <td>Connaissance VMware, VirtualBox, KVM/XEN</td>
        </tr>
        <tr>
          <td class="cell-left">Base de donnée</td>
          <td>MySQL, PostgreSQL</td>
        </tr>
        <tr>
          <td class="cell-left">Système de gestion de contenu</td>
          <td>Wordpress, Joomla, Spip<br />Maîtrise des différents outils de communications</td>
        </tr>
        <tr>
          <td class="cell-left">Composants</td>
          <td>Maîtrise des différents composants système, de la conception de machines bureautique ou portable.<br />Compétences en dépannage et détection de panne matériels.</td>
        </tr>
        <tr>
          <td class="cell-left">Bureautique</td>
          <td>Compétences des différents outils : Word, Excel, Access et OpenOffice.<br />Bonne connaissance FileMaker.</td>
        </tr>
        <tr>
          <td class="cell-left">Graphisme</td>
          <td>Connaissance de base en graphisme pour création de site internet.<br />Bonne connaissance des logiciels : Gimp, Scribus.</td>
        </tr>
      </tbody>
    </table>

    <div id="CV-CMP-Indus" class="section indigo darken-4 white-text z-depth-2" >
      <h2>COMPÉTENCES INDUSTRIELLES</h2>
    </div>
    <table class="striped grey lighten-5 z-depth-2">
      <tbody>
        <tr>
          <td class="cell-left">Conventionnelle</td>
          <td>Bonne maîtrise de machines outils sur fraiseuse/tour, condition de coupe, respect des spécifications d'usinage</td>
        </tr>
        <tr>
          <td class="cell-left">Numérique</td>
          <td>Machines : Bonne maîtrise sur machine outil DMU, Hermle, Mazak...</td>
        </tr>
        <tr>
          <td class="cell-left">Outil</td>
          <td>Bonne connaissance des divers outils et conditions de coupe pour machine conventionnelle et numérique.<br />Bonne connaissance du montage d'outils en pince ou frettage.</td>
        </tr>
        <tr>
          <td class="cell-left">Programmation</td>
          <td>Bonne connaissance programmation ISO et MazakTrol.</td>
        </tr>
        <tr>
          <td class="cell-left">Contrôle</td>
          <td>Maîtrise des différents moyen de contrôle manuel.<br />Bonne connaissance de machine Tridimensionnelle.</td>
        </tr>
      </tbody>
    </table>

    <div id="CV-Autres" class="section indigo darken-4 white-text z-depth-2">
      <h2>AUTRES COMPÉTENCES</h2>
    </div>
    <table class="striped grey lighten-5 z-depth-2">
      <tbody>
        <tr>
          <td class="cell-left">Langues</td>
          <td>Anglais : niveau scolaire, bon niveau technique informatique.</td>
        </tr>
      </tbody>
    </table>

    <div id="CV-Loisirs" class="section indigo darken-4 white-text z-depth-2">
      <h2>LOISIRS</h2>
    </div>
    <table class="striped grey lighten-5 z-depth-2">
      <tbody>
        <tr>
          <td class="cell-left">Permis</td>
          <td>A et B</td>
        </tr>
        <tr>
          <td class="cell-left">Sports</td>
          <td>Vélo, roller...</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
</div>

<!-- Footer -->
<footer id="liens" class="page-footer amber darken-2 z-depth-2">
    <div class="container">
        <div class="row">
            <div class="col l3 s12">
                <h5 class="white-text">Distributions utilisées</h5>
                <ul>
                  <li><a href="https://www.debian.org/" title="Debian">Debian</a></li>
              		<li><a href="https://www.ubuntu.com/" title="Ubuntu">Ubuntu</a></li>
            		  <li><a href="https://almalinux.org/" title="AlmaLinux">AlmaLinux</a></li>
              		<li><a href="https://linuxmint.com/" title="Linux Mint">Linux Mint</a></li>
              		<li><a href="https://getfedora.org/" title="Fedora Project">Fedora Project</a></li>
              		<li><a href="https://www.gentoo.org/" title="Gentoo Linux">Gentoo Linux</a></li>
              		<li><a href="https://www.openmediavault.org/" title="OpenMediaVault">OpenMediaVault</a></li>
              		<li><a href="https://yunohost.org/" title="YunoHost">YunoHost</a></li>
                </ul>
            </div>
            <div class="col l3 s12">
                <h5 class="white-text">Divers et contributions</h5>
                <ul>
                  <li><a href="https://www.ovh.com/fr/" title="OVH : hébergement, nom de domaine, serveur dédié, CDN, Cloud, Big Data, ...">OVH</a></li>
                  <li><a href="https://www.pulseheberg.com/" title="Hébergeur de sites web, d'infrastructures dédiées, de serveurs virtuels (VPS) de haute qualité Windows & Linux. Réseau indépendant (AS62000)">Pulseheberg</a></li>
                  <li><a href="http://debian-handbook.info/" title="The Debian Administrator's Handbook">The Debian Administrator's Handbook</a></li>
            			<li><a href="http://syncthing.net/" title="Syncthing">Syncthing</a></li>
            			<li><a href="https://opnsense.org/" title="OPNsense">OPNsense</a></li>
                </ul>
            </div>
            <div class="col l3 s12">
                <h5 class="white-text">Autres liens</h5>
                <ul>
                  <li><a href="https://www.tifred.fr/" title="TiFredFr">TiFredFr Blog</a></li>
                  <li><a href="https://shaarli.tifred.fr/" title="TiFredFr">TiFredFr Shaarli</a></li>
            			<li><a href="https://gitlab.com/tifredfr" title="GitLab">TiFredFr GitLab</a></li>
            			<li><a href="https://github.com/tifredfr" title="GitHub">TiFredFr GitHub</a></li>
            			<li><a href="https://framagit.org/tifredfr" title="FramaGit">TiFredFr FramaGit</a></li>
            			<li><a href="http://www.istack.fr/" title="iStack">iStack</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright grey darken-4">
        <div class="container">
            <small>Page mise à disposition selon les termes <a class="amber-text" href="https://creativecommons.org/licenses/by-nc/4.0/">CC BY-NC</a> | Made by <a class="amber-text" href="http://materializecss.com">Materialize</a></small>
            <div class="right"><a class="amber-text" href="https://gitlab.com/flietart/www.lietart.fr"><small><i class="fa fa-refresh"></i> 04/06/2022</a> <i class="fa fa-copyright"></i> Frédéric Lietart</small></div>
        </div>
    </div>
</footer>
<!-- /footer -->

<!--  Scripts-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script type="text/javascript" src="js/init.js"></script>
<?php include_once("analyticstracking.php") ?>
</body>
</html>
